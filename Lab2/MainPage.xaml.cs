﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<CustomCellView> objects = new ObservableCollection<CustomCellView>();
        public MainPage()
        {
            InitializeComponent();
            PopulateResults();
        }

        private void PopulateResults()
        {
            objects.Add(new CustomCellView { LabelTeam1 = "Chelsea 0", LabelTeam2 = "4 Bayern", SrcImage1 = "chelsea.png", SrcImage2 = "bayern.png" });
            objects.Add(new CustomCellView { LabelTeam1 = "Lyon 1", LabelTeam2 = "0 Juventus", SrcImage1 = "lyon.png", SrcImage2 = "juventus.png" });
            objects.Add(new CustomCellView { LabelTeam1 = "Napoli 1", LabelTeam2 = "1 Barcelona", SrcImage1 = "napoli.png", SrcImage2 = "barcelona.png" });
            objects.Add(new CustomCellView { LabelTeam1 = "Real Madrid 1    ", LabelTeam2 = "2 Man. City", SrcImage1 = "realmadrid.png", SrcImage2 = "mancity.png" });
            ListView.ItemsSource = objects;
        }

        private void ListView_Refreshing(object sender, EventArgs e)
        {
            objects.Add(new CustomCellView { LabelTeam1 = "Barcelona 10    ", LabelTeam2 = "0 Real Madrid", SrcImage1 = "barcelona.png", SrcImage2 = "realmadrid.png" });
            objects.Add(new CustomCellView { LabelTeam1 = "Chelsea 10    ", LabelTeam2 = "0 Real Madrid", SrcImage1 = "chelsea.png", SrcImage2 = "realmadrid.png" });
            ListView.IsRefreshing = false;
        }
    }
}
